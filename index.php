<?php
	get_header();
?>
<section class="main">
	
		<section class="main__block main__block--slider">
			<div class="block-cnt block-cnt--slider">
				<header class="block-cnt__header"><h4>Important News</h4></header>
				<div class="block-cnt__inner block-cnt__inner--important-news">
					<div class="owl-carousel">					  
						<?php
							$args = array( 'post_type' => 'important_news', 'posts_per_page' => 4 );
							$loop = new WP_Query( $args );
							if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
								if(has_post_thumbnail()): ?>
									<?php if(get_post_format() !== 'video'): ?>
										<div class="owl-carousel-item">
											<a class="owl-carousel-item__link" href="<?php the_permalink() ?>">
												<header class="owl-carousel-item__link__header"> <h3> <?php the_title();?> </h3></header>
												<?php the_post_thumbnail('large-thumnail', $attr = array(
												    'class' => "owl-lazy",
												    'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumnail') )
												);
												?>
											</a>
										</div>
									<?php else :?>
										<div class="owl-carousel-item">
											<a class="owl-carousel-item__link" href="<?php the_permalink() ?>">
												<header class="owl-carousel-item__link__header"> <h3> <?php the_title();?> </h3></header>
												<span class="medium-news-post-thumnail__play-btn--left company news-play-button"></span>
												<?php the_post_thumbnail('large-thumnail', $attr = array(
												    'class' => "owl-lazy",
												    'data-src' => get_the_post_thumbnail_url( get_the_ID(), 'large-thumnail') )
												);
												?>
											</a>
										</div>
									<?php endif; ?>	
								<?php else : ?>
	                                <div class="owl-carousel-item">
										<a class="owl-carousel-item__link" href="<?php the_permalink(); ?>">
											<header class="owl-carousel-item__link__header"><h3> <?php the_title();?> </h3></header>
											<?php 
												$upload_dir = wp_upload_dir();  
												$upload_dir['url'];
												set_post_thumbnail($post->ID, attachment_url_to_postid($upload_dir['url'] . '/breaking.jpg'));
											?>
										</a>
									</div>
							<?php endif; 
							/* Restore original Post Data */
								// wp_reset_postdata();
								endwhile;
								else: ?>
                                   <img src="<?php echo get_template_directory_uri()?>/images/no-news.jpg" alt="No important news" />
							<?php endif;?>
					</div>
				</div>
				<div class="block-cnt__buttons-cnt">
					<button class="block-cnt__buttons-cnt__button" id="go-bottom-button"><span>About</span></button>	
				    <button class="block-cnt__buttons-cnt__button" id="go-info-button"><span>More Info</span></button>	
				</div>
				
			</div>
			
		</section>

		<section class="main__block">
			<div class="block-cnt">
				<header class="block-cnt__header"><h4>Lorem ipsum dolor sit amet</h4></header>
				<div class="block-cnt__inner block-cnt__inner--pictures">
					<figure class="info-pictures-cnt__picture info-pictures-cnt__picture--one">
						<span class="info-pictures-cnt__picture__infobox">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut
						</span>
					</figure>

					<figure class="info-pictures-cnt__picture info-pictures-cnt__picture--two">
						<span class="info-pictures-cnt__picture__infobox">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut
						</span>
					</figure>

					<figure class="info-pictures-cnt__picture info-pictures-cnt__picture--three">
						<span class="info-pictures-cnt__picture__infobox">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut
						</span>
					</figure>
				</div>
			</div>
		</section>

		<section class="main__block">
			<div class="block-cnt">
				<header class="block-cnt__header" id="info"><h4>More information</h4></header>
				<article class="block-cnt__inner block-cnt__inner--info">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</article>
			</div>	
		</section>

		<section class="main__block">
			<div class="block-cnt">
				<header class="block-cnt__header"><h4>Latest News</h4></header>
				<div class="block-cnt__inner block-cnt__inner--news">
					<?php
						if (have_posts()) :
							while (have_posts()) : the_post(); ?>
								<article class="news">
									<div class="news__inner-news-post-cnt <?php if(has_post_thumbnail()): ?> news__inner-news-post-cnt--clear <?php endif; ?>">
										<h4 class="news-post-header"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
										<!-- time category author start -->
										<div class="news-post-info">
											Posted in <?php $categories = get_the_category(); 
											$separetor = ", ";
											$output = '';
											if($categories){
												foreach($categories as $category){
													$output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
												}
												echo trim($output, $separetor);
											}
											?>
												<?php the_time('F j, Y'); ?>
												<?php edit_post_link(
														sprintf(
															/* translators: %s: Name of current post */
															__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'company-th' ),
															get_the_title()
														),
														'<span class="edit-link-p">',
														'</span>'
													); ?> 
										</div>
										<?php if(has_post_thumbnail()): ?>
											<?php if(get_post_format() == 'video'){ ?>
												<div class="small-news-post-thumnail">
													<a href="<?php the_permalink() ?>">
													    <span class="small-news-post-thumnail__play-btn company news-play-button"></span>
													    <?php the_post_thumbnail('small-thumnail');?>
												    </a>
			                                    </div>
											<?php } else { ?>
											<div class="small-news-post-thumnail">
												<a href="<?php the_permalink() ?>">
													<?php the_post_thumbnail('small-thumnail');?>
												</a>
											</div>
											
										<?php } endif; ?>

										
			                       
										<div class="news-post-excerpt">
											<?php 
												
												the_excerpt();
												
											?>
										</div>
								    </div>
								</article>
								<?php wp_reset_postdata(); ?>
							<?php endwhile;?>


							<?php
							
						else :
							echo '<h3 style="text-align: center;">No posts so far( </h3>';
						endif;
					?>
					<div class="pagination_cnt">
						<div class="pagination_cnt__inner">
							<?php echo paginate_links(array(
								'next_text' => '<span class="right-arrow company comp-circle-right"></span>',
								'prev_text' => '<span class="left-arrow company comp-circle-left"></span>'
								));?>
						</div>
					</div> 
				</div>
			</div>
		</section>
</section>

<?php		
	get_footer();
?>