<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search' ); ?>" title="search" autocomplete="off"/>
	<span class="search-btn company comp-search">
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="" title="submit"/>
	</span>
</form>