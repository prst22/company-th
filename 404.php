<?php
	get_header();
?>
<section class="main">
	<section class="main__block">
		<div class="block-cnt">
			<!-- <div class="post-cnt column" style="justify-content: center; min-height: inherit;"> -->
				<div class="search-form-cnt" style="width: 100%;"><?php get_search_form(); ?></div>
				<h3 style="text-align: center; width: 100%; padding-left: 2rem; padding-right: 2rem;">Something went wrong. Please try again</h3>
				<h1 style="text-align: center; font-weight: 700; width: 100%; padding-left: 2rem; padding-right: 2rem;">404</h1>
		    
		</div>
	</section>
</section>
<?php		
	get_footer();
?>
