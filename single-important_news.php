<?php
get_header();
?>
<section class="main">
	<section class="main__block">
		<div class="block-cnt">
		<?php 
			if (have_posts()) :
			while (have_posts()) : the_post();?>
					<div class="inner-single-post-cnt">
						<?php if(has_post_thumbnail()): ?>

							<div class="single-post-thumbnail-cnt single-post-thumbnail-cnt--important-news">
								<?php the_post_thumbnail('large-thumnail', $attr = array(
								    'class' => "single-post-thumbnail-cnt__image")
								);?>
							</div>
		
						<?php endif;?>
						<h3 class="single-post-cnt__header <?php if(has_post_thumbnail()): ?> single-post-cnt__header--clear <?php endif; ?>"><?php the_title(); ?></h3>
                        <div class="single-post-cnt__post-info">
							<span class="news-post-info__important-news">
								<a href="<?php echo get_page_link( get_page_by_path( 'all-important-news' )->ID ); ?>">
									Important news
								</a>
							</span>
							<span class="news-post-info__important-news-date">
								<?php the_time('F j, Y'); ?>
								<?php edit_post_link(
										sprintf(
											/* translators: %s: Name of current post */
											__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'company-th' ),
											get_the_title()
										),
										'<span class="edit-link-p">',
										'</span>'
									); ?> 
							</span>
						</div> 
						<!-- time category author start -->
							<!-- time category author start  end-->
							<article class="single-post-cnt__post-content">
								<?php 
									the_content();
								?>
							</article>
					    </div>
				
			<?php endwhile;
		    else : ?>
				<h3 style="text-align: center;"><?php __('No post found') ?></h3>
			<?php endif; ?>
			<?php if ( (comments_open() || get_comments_number())) :?>
				<div class="comment-cnt">
				    <?php comments_template(); ?>
				</div>
			<?php endif; ?>
	    </div>
	</section>
</section>
<?php		
get_footer();
?>