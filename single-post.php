<?php
get_header();
?>
<section class="main">
	<section class="main__block">
		<div class="block-cnt">
				<?php 
					if (have_posts()) :
					while (have_posts()) : the_post(); ?>
						<?php get_template_part('content', get_post_format()); ?>
						
					<?php endwhile;
				    else : ?>
					<h3 style="text-align: center;"><?php __('No post found') ?></h3>
					<?php endif;
				?>
				<?php
					if ( (comments_open() || get_comments_number())) :?>
					<div class="comment-cnt">
					    <?php comments_template(); ?>
					</div>
					<?php endif; ?>
		</div>
	</section>
</section>
<?php		
get_footer();
?>