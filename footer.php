		<footer class="footer">
			<div class="footer__inner">
				<div class="about" id="footer">
					<h4 class="about__header">About</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.</p>	
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.</p>
				</div>

				<nav class="footer-menu">
					<?php
					$args = array(
						'theme_location' => 'footer_menu'
					);
					 wp_nav_menu($args); ?>

	 				<div class="footer-menu__footer-copy">
						<a href="<?php echo home_url();?>"><?php bloginfo('name') ?></a> - &copy; <?php echo date("Y");?>	
					</div> 
				</nav>

				    <?php  if(is_active_sidebar('footer-search')){ 
				            dynamic_sidebar('footer-search');
			            }
				    ?>	
				<div class="footer-logo"></div> 
  
	        </div>
			<div class="go-top-button">
				<a href="#" class="go-top-button__link company comp-circle-up" id="go-top" title="Go top"></a>
			</div>
		</footer>
	</div>
</div>
<?php wp_footer(); ?>
</body>

</html>
