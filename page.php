<?php get_header();?>
<section class="main">
	<section class="main__block">
		<div class="block-cnt">
			<div class="single-page-cnt">
				<?php  
				if (have_posts()) :
					while (have_posts()) : the_post(); ?>
						
						<header class="single-page-cnt__header"><h3><?php the_title(); ?></h3></header>
						<article class="single-page-cnt__content"><?php the_content(); ?></article>

				<?php endwhile;
				else :?>
					<h3 style="text-align: center;"><?php __('No page found') ?></h3>
					<?php endif; ?>
		    </div>
		</div>
	</section>
</section>
<?php get_footer();?>