<!-- get content part -->
<div class="single-post-cnt">
	<h4 class="single-post-cnt__header"><?php the_title(); ?></h4>
	<!-- time category author start -->
       <div class="single-post-cnt__post-info">
			Posted in <?php $categories = get_the_category(); 
			$separetor = ", ";
			$output = '';
			if($categories){
				foreach($categories as $category){
					$output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
				}
				echo trim($output, $separetor);
			}
			?>
			<?php the_time('F j, Y'); ?>
			<?php edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'company-th' ),
					get_the_title()
				),
				'<span class="edit-link-p">',
				'</span>'
			); ?> 
		</div>
	<!-- time category author start  end-->
	<article class="single-post-cnt__post-content">
		<?php 
			the_content();
		?>
	</article>
</div>