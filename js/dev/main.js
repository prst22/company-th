!function(){
    let menu = document.getElementsByClassName("nav-menu-cnt")[0],
	// pElementsCnt = document.querySelectorAll(".post-content>p"),
	temporaryProgress = 0,
	temporaryProgress2 = 0, 
	requestAnimationFrameStart = null, 
	requestAnimationFrameEnd = null, 
	btn = document.querySelector('#btn'),
	smooth = linear,
	menuLine = document.getElementsByClassName('nav-block__line'),
	goFooter = document.querySelector('.go-to-footer>a'),
	scrollContainer = document.querySelector('.site-wrap'),
	goToInfoButton = document.getElementById('go-info-button'),
	goToBottomButton = document.getElementById('go-bottom-button'),
	rotation = 0;
	// controll font loading start
	let font1 = new FontFaceObserver('Economica', {
		  weight: 400
	});
	let font2 = new FontFaceObserver('Economica', {
		  weight: 700
	});
	var html = document.documentElement;

	html.classList.add('fonts-loading');

	Promise.all([font1.load(null, 5000), font2.load(null, 5000)]).then(function () {
		html.classList.remove('fonts-loading');
		html.classList.add('fonts-loaded');
		sessionStorage.fontsLoaded = true;
	}).catch(function () {
		html.classList.remove('fonts-loading');
		html.classList.add('fonts-failed');
		sessionStorage.fontsLoaded = false;
	});
	// controll font loading start end

	// media query
	function controllVpSize(x) {
	    if (x.matches) { // If media query matches
	        btn.style.cssText = '';
	        menu.style.cssText = '';
            menuLine[0].style.cssText = '';
            menuLine[1].style.cssText = '';
            menuLine[2].style.cssText = '';
	        if(menu.classList.contains("nav-menu-cnt--show")){
		        closeMenu();
            }
	    } else if( !x.matches){
	    	btn.style.cssText = '';
	        menu.style.cssText = '';
            menuLine[0].style.cssText = '';
            menuLine[1].style.cssText = '';
            menuLine[2].style.cssText = '';
            if(menu.classList.contains("nav-menu-cnt--show")){
            	closeMenu();
            }
	    }
	}
	let x = window.matchMedia("(max-width: 1325px)");
	controllVpSize(x); // Call listener function at run time
	x.addListener(controllVpSize);
	// media query end  
	// menu start
	function animate(options) {
		cancelAnimationFrame(requestAnimationFrameEnd);
		let start = performance.now();
		requestAnimationFrame(function animate(time) {
			let timeFraction = temporaryProgress2;
			timeFraction += ( performance.now() - start ) / options.duration;
			if (timeFraction > 1) timeFraction = 1;
			let progress = options.timing(timeFraction);
			options.draw(progress);
			if (timeFraction < 1) {
				requestAnimationFrameStart =  requestAnimationFrame(animate);
			}
			temporaryProgress = progress; // if menu is open/closed before it fully oppened
		});
	}
	function animateBack(options){
		cancelAnimationFrame(requestAnimationFrameStart);
		let start = performance.now();
	    requestAnimationFrame(function animate(time) {
	    	let timeFraction = temporaryProgress;
			timeFraction -= ( performance.now() - start ) / options.duration;
			if (timeFraction < 0) timeFraction = 0;
			let progress = options.timing(timeFraction);
			options.draw(progress);
			if (timeFraction > 0) {
				requestAnimationFrameEnd = requestAnimationFrame(animate);
			}
			temporaryProgress2 = progress; // if menu is open/closed before it fully oppened
		});
	}
	function linear(timeFraction) {
		return timeFraction;
	}
	btn.addEventListener('click', function() {
	    if (menu.classList.contains("nav-menu-cnt--hidden")) openMenu();
	    if (menu.classList.contains("nav-menu-cnt--show")) closeMenu();
	});
	function openMenu(){
		animate({
		    duration: 390,
		    timing: smooth,
		    draw: function(progress) {
		    	if (!x.matches){
		    		btn.style.transform = "rotate(" + (progress * 180) + "deg) translateY(-50%)"; 
			        menu.style.width = (progress * 100) + "%";
	                menuLine[0].style.height = (progress * 100) + "%";
	                menuLine[1].style.height = (progress * 100) + "%";
	                menuLine[2].style.height = (progress * 100) + "%";
			        menu.classList.remove("nav-menu-cnt--hidden");
			        menu.style.pointerEvents = "auto";
			        menu.classList.add("nav-menu-cnt--show");
			        menuScrollBar.update();
			    } else {
			    	btn.style.transform = "rotate(" + (progress * 180) + "deg) translateY(0%) translateX(-50%)";
			    	menu.style.height = "calc(" + (progress * 100) + "% - 80px)";
			    	menu.style.minHeight = "calc(" + (progress * 100) + "% - 80px)";
			    	menuLine[0].style.width = (progress * 100) + "%";
	                menuLine[1].style.width = (progress * 100) + "%";
	                menuLine[2].style.width = (progress * 100) + "%";
			    	menu.classList.remove("nav-menu-cnt--hidden");
			        menu.style.pointerEvents = "auto";
			        menu.classList.add("nav-menu-cnt--show");
			        menuScrollBar.update();
			    }
			}    
		});
	}
	function closeMenu(){
		animateBack({
	        duration: 390,
	        timing: smooth,
	        draw: function(progress) {
	        	if(!x.matches){
	        		btn.style.transform = "rotate(" + (progress * 180) + "deg) translateY(-50%)"; 
		            menu.style.width = (progress * 100) + "%";
		            menuLine[0].style.height = (progress * 100) + "%";
		            menuLine[1].style.height = (progress * 100) + "%";
		            menuLine[2].style.height = (progress * 100) + "%";
		            menu.classList.add("nav-menu-cnt--hidden");
		            menu.classList.remove("nav-menu-cnt--show");
		            menu.style.pointerEvents = "none";
	            } else {
                    btn.style.transform = "rotate(" + (progress * 180) + "deg) translateY(0%) translateX(-50%)";
			    	menu.style.height = (progress * 100) + "%";
			    	menu.style.minHeight = (progress * 100) + "%";
			    	menuLine[0].style.width = (progress * 100) + "%";
	                menuLine[1].style.width = (progress * 100) + "%";
	                menuLine[2].style.width = (progress * 100) + "%";
			    	menu.classList.add("nav-menu-cnt--hidden");
		            menu.classList.remove("nav-menu-cnt--show");
		            menu.style.pointerEvents = "none";
			    }  
	        }
	    });	
	}
	document.onkeydown = function(evt) {
		evt = evt || window.event;
		let isEscape = false;
		if ("key" in evt) {
			isEscape = (evt.key == "Escape" || evt.key == "Esc");
		} else {
			isEscape = (evt.keyCode == 27);
		}
		if (isEscape) {
			if (menu.classList.contains("nav-menu-cnt--show")) closeMenu();
	    }
	}
	// menu end
    
    // important news slider start
	jQuery(document).ready(function(){
		jQuery('.owl-carousel').owlCarousel();
	});

	jQuery('.owl-carousel').owlCarousel({
	    loop: true,
	    lazyLoad: true,
	    checkVisible: false,
	    items: 1,
	    nav: false,
	    dots: false,
	    autoplay: true,
	    autoplayTimeout: 5000
	});
	// important news slider end
    // smooth scroll start
    if(goFooter){
		goFooter.addEventListener('click', closeMenu);
		goFooter.addEventListener('click', function(e){
	    	e.preventDefault();
		        TweenLite.to(scrollContainer, 2, {scrollTo:{y:"max", autoKill: true}});
			}
	    );
	}
	if (goToBottomButton) {
		goToBottomButton.addEventListener('click', function(){
		        TweenLite.to(scrollContainer, 2, {scrollTo:{y:"max", autoKill: true}});
			}
	    );
	}
    if(goToInfoButton){
    	goToInfoButton.addEventListener('click', function(){
		        TweenLite.to(scrollContainer, 2, {scrollTo:{y: document.getElementById('info').offsetTop, autoKill: true}});
			}
	    );
    }
    document.getElementById('go-top').addEventListener('click', function(){
	        TweenLite.to(scrollContainer, 2, {scrollTo:{y:scrollContainer.offsetTop, autoKill: true}});
		}
    );
    // smooth scroll end
    let scrollbarMenuCnt = document.querySelector('.nav-menu-cnt__scroll-cnt');
    menuScrollBar = Scrollbar.init(scrollbarMenuCnt, 
	    ScrollbarOptions = {
	    alwaysShowTracks: true,
	});
}(); 
		// window.onclick=function (x){
		// 		if(!x.target.matches("#menu-primary * ") 
		// 			&& !x.target.matches("#btn") 
		// 			&& !x.target.matches(".line")){
		// 			menu.classList.remove("show");
		// 			menu.classList.add("hide");
		// 	}
		// 	if(!x.target.matches(".menu-item-has-children * ")){
		// 		subDrop[0].classList.remove("show-sub-drop");
		// 		subDrop[0].classList.add("hide-sub-drop");
		// 	}
		// }
		// subDropTrigger[0].addEventListener("click", function(e){

		// 	if(subDrop){
		// 		if(!(subDrop[0].classList.contains("show-sub-drop"))) {
		// 				subDrop[0].classList.add("show-sub-drop");
		// 				subDrop[0].classList.remove("hide-sub-drop");
		// 				e.preventDefault();
		// 			}
		// 			else {
		// 				subDrop[0].classList.remove("show-sub-drop");
		// 				subDrop[0].classList.add("hide-sub-drop");
		// 				e.preventDefault();
		// 			}
		// 		}
		// });
		// let matchMe = window.matchMedia("(max-width: 900px)"); 
		// matchMe.addListener(closeMenuOnResize);
		// function closeMenuOnResize(){
		// 	if(matchMe.matches || !matchMe.matches){
		// 		if(menu && menu.classList.contains("show")) {
		// 			menu.classList.add("hide");
		// 			menu.classList.remove("show");
		// 		}
		// 		if(subDrop && subDrop[0].classList.contains("show-sub-drop")) {
		// 				subDrop[0].classList.add("hide-sub-drop");
		// 				subDrop[0].classList.remove("show-sub-drop");
		// 		}
		// 	}
		// }
		// if (Element && !Element.prototype.matches){
		//     var proto = Element.prototype;
		//     proto.matches = proto.matchesSelector ||
		//         proto.mozMatchesSelector || proto.msMatchesSelector ||
		//         proto.oMatchesSelector || proto.webkitMatchesSelector;
		// }
		// function addClassToFirstPwithText(){
		// 	let bool = true;
		// 	for (let e = 0; e < pElementsCnt.length; e++) {
		// 	    if (pElementsCnt[e] && bool === true) {    	
		// 			searchForTextInP(e);
		// 		}else{
		// 			break;
		// 		}
		// 	}
		//     function searchForTextInP(e){
		// 		let children = pElementsCnt[e].childNodes;
		// 			for (let i = 0; i < children.length; i++) {
		// 			  	if (children[i].nodeType === 3 && children[i].nodeValue != "") {
		// 			  		let amountOfWords = WordCount(children[i].nodeValue);
		// 			  		if(amountOfWords >= 25){
		// 			  			pElementsCnt[e].classList.add("first-p-with-text");
		// 			            bool = false;
		// 			            break;   
		// 			  		}else{
		// 			  			bool = false;
		// 			  			break;
		// 			  		}
				            
		// 		        }
		// 		    }
		// 	}
		// }
		// function WordCount(str) { 
		//   return str.split(" ").length;
		// }

		// if(pElementsCnt){
		//     addClassToFirstPwithText();
		// }
		// if (/MSIE 10/i.test(navigator.userAgent)) {
		// 	document.documentElement.style.backgroundColor = "#262626";
		// }
		// if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
		//     document.documentElement.style.backgroundColor = "#262626";
		// }
		//  if (/Edge\/\d./i.test(navigator.userAgent)){
		//     document.documentElement.style.backgroundColor = "#262626";
		// }
		
		// let link = document.querySelectorAll("#menu-primary>li>a");
		// let loadingEl = document.getElementsByClassName("loading-el");
		// if (link) {
		// 	document.addEventListener("DOMContentLoaded", function(event){
			 
		// 		for(i = 0; i < link.length; i++){
		// 			link[i].removeChild(loadingEl[0]);
		// 		}
				
		// 	});
		// }
		// document.addEventListener('touchstart', function addtouchclass(e){ // first time user touches the screen
	 //    document.documentElement.classList.add('can-touch') // add "can-touch" class to document root using classList API
	 //    document.removeEventListener('touchstart', addtouchclass, false) // de-register touchstart event
		// }, false)


