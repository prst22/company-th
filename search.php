<?php
/*
Template Name: Search Page
*/
?>
<?php
	get_header();
?>
<section class="main">
	<section class="main__block">
		<div class="block-cnt">
				<div class="search-form-cnt"><?php get_search_form(); ?></div>
				<h3 style="padding: 0 1.5rem">Search results</h3>
				<div class="block-cnt__inner block-cnt__inner--news">
					<?php
						if (have_posts()) :
							while (have_posts()) : the_post(); ?>
								<article class="news">					
									<div class="news__search-post-cnt <?php if(has_post_thumbnail()): ?> news__search-post-cnt--clear <?php endif; ?>">
										<h4 class="news-post-header"><a href="<?php the_permalink() ?>"><?php the_title();?></a>
										</h4>
										<?php if($post->post_type !== 'page'):?>
										<!-- time category author start -->
										<div class="news-post-info">
											Posted in <?php $categories = get_the_category(); 
											$separetor = ", ";
											$output = '';
											if($categories){
												foreach($categories as $category){
													$output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
												}
												echo trim($output, $separetor);
											}
											?>
											<?php the_time('F j, Y'); ?>
									    </div>
									<!-- time category author start  end-->
									<?php if(has_post_thumbnail()): ?>
										<?php if(get_post_format() == 'video'){ ?>
											<div class="small-news-post-thumnail">
												<a href="<?php the_permalink() ?>">
												    <span class="small-news-post-thumnail__play-btn company news-play-button"></span>
												    <?php the_post_thumbnail('small-thumnail');?>
											    </a>
		                                    </div>
		                                    <?php } else { ?>
										<div class="small-news-post-thumnail">
											<a href="<?php the_permalink() ?>">
												<?php the_post_thumbnail('small-thumnail');?>
											</a>
										</div>
										
									<?php } endif; ?>

									<div class="search-post-excerpt">
										<?php the_excerpt(); ?>
									</div>
									<?php else: ?>
										<div class="search-post-excerpt">
											<?php the_excerpt(); ?>
										</div>
									<?php endif; ?>
							    </div>
							</article>
						<?php endwhile;?>
						<?php
							else :
								echo '<h3 style="text-align: center;">No matches found( </h3>';
							endif;
						?>
				<div class="pagination-cnt">
					<div class="pagination-cnt__inner">
						<?php echo paginate_links(array(
							'next_text' => '<span class="right-arrow company comp-circle-right"></span>',
							'prev_text' => '<span class="left-arrow company comp-circle-left"></span>'
							));?>
					</div>
				</div> 		
			</div>
	</section>
</section>
<?php		
	get_footer();
?>

