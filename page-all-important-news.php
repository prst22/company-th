<?php
get_header();
?>
<section class="main">
	<section class="main__block">
		<div class="block-cnt">
			<header class="block-cnt__header"><h3>All Important News</h3></header>
			<div class="block-cnt__inner block-cnt__inner--news">
			<?php
			    $translated = __( 'Page', 'company-th' ); // Supply translatable string
				$big = 999999999;
				//Protect against arbitrary paged values
				$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

				$args = array( 'post_type' => array('important_news'), 'posts_per_page' => 10, 'paged' => $paged);
				$loop_for_all_important_news = new WP_Query( $args );
					if ($loop_for_all_important_news->have_posts()) :
						while ($loop_for_all_important_news->have_posts()) : $loop_for_all_important_news->the_post(); ?>
							<article class="news">
								<div class="news__inner-news-post-cnt news__inner-news-post-cnt--all-news">
									<h4 class="all-news-post-header <?php if(get_post_type() === 'important_news'): ?> all-news-post-header--important-news<?php endif; ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
									<!-- time category author start -->
									<div class="news-post-info">
											<span class="news-post-info__important-news">
												<a href="<?php echo get_page_link( get_page_by_path('all-important-news')->ID); ?>">Important news</a>
											</span>
											<span class="news-post-info__important-news-date">
												<?php the_time('F j, Y'); ?>
												<?php edit_post_link(
														sprintf(
															/* translators: %s: Name of current post */
															__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'company-th' ),
															get_the_title()
														),
														'<span class="edit-link-p">',
														'</span>'
													); ?> 
											</span>	
									</div>
									<div class="all-news-content-wrap">
										<?php if(has_post_thumbnail()): ?>
											<?php if(get_post_format() == 'video'){ ?>
												<div class="medium-news-post-thumnail">
													<a href="<?php the_permalink() ?>">
													    <span class="medium-news-post-thumnail__play-btn company news-play-button"></span>
													    <?php the_post_thumbnail('medium-thumnail');?>
												    </a>
			                                    </div>
											<?php } else { ?>
											<div class="medium-news-post-thumnail">
												<a href="<?php the_permalink() ?>">
													<?php the_post_thumbnail('medium-thumnail');?>
												</a>
											</div>
											
										<?php } endif; ?>

										<div class="news-post-excerpt news-post-excerpt__all-news-page <?php if(!has_post_thumbnail()): ?> news-post-excerpt__all-news-page--text-only <?php endif; ?>">
											<?php 
												the_excerpt();
											?>
										</div>
									</div>
							    </div>
							</article>
							<?php wp_reset_postdata(); ?>
						<?php endwhile;?>
	   
						<?php
						else :
							echo '<h3 style="text-align: center;">No posts so far( </h3>';
						endif;
				
		    ?>
		    <div class="pagination-cnt">
				<div class="pagination-cnt__inner">
					<!-- additional parameters for paginate_links() to paginate custom loop(new $WP_Query)on page are needed -->
					<?php echo paginate_links(array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $loop_for_all_important_news->max_num_pages,
						'next_text' => '<span class="right-arrow company comp-circle-right"></span>',
						'prev_text' => '<span class="left-arrow company comp-circle-left"></span>',
						'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>'          
						));?>
				</div>
			</div>
			</div>
		</div>
	</section> 
</section>
<?php		
get_footer();
?>