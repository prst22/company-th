const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const sass        = require('gulp-sass');
const concat = require('gulp-concat');

// Compile Sass & Inject Into Browser
gulp.task('sass', function() {
    return gulp.src(['scss/*.scss'])
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("css"))
        .pipe(browserSync.stream());
});
gulp.task('scripts', function() {
  return gulp.src(['js/dev/smooth-scrollbar.js','js/dev/owl/owl.carousel.min.js', 'js/dev/TweenLite.min.js', 'js/dev/ScrollToPlugin.min.js','js/dev/main.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('js/dist/'));
});
// Watch Sass & Serve
gulp.task('serve', ['sass', 'scripts'], function() {
    browserSync.init({
		proxy: "http://localhost/",
		notify: false
	});
    gulp.watch(['scss/*.scss'], ['sass']);
    gulp.watch("*.php").on('change', browserSync.reload);
    gulp.watch("js/dev/*.js").on('change', browserSync.reload);
});

// Default Task

gulp.task('default', ['serve']);