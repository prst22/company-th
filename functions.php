<?php

	//include walker class start

	require_once get_template_directory() . '/walker.php';
    
    //include walker class end

	//add theme support
	function add_q() {
		// Adding menus to wp controll pannel
		register_nav_menus(array(
			'main_header_menu' =>__('Main header menu'),
			'secondary_header_menu' =>__('Secondary header menu'),
			'footer_menu' =>__('Footer Menu')
		));

	    //post thum pictures
	    add_theme_support('post-thumbnails');

	    add_image_size('small-thumnail', 180, 90, array('center','center'));
	    add_image_size('medium-thumnail', 500, 250, array('center','center'));  
	    add_image_size('large-thumnail', 1366, 683, array('center','center'));
	   
	    add_theme_support( 'post-formats', array( 'video' ));
   
	    //CUSTOM WORDPRESS EDITOR STYLE
		add_editor_style( 'css/custom-editor-styles.css' );
	    //CUSTOM WORDPRESS EDITOR STYLE 
	    
	}
	add_action('after_setup_theme', 'add_q');

    // rename post into news start
	// add_filter('post_type_labels_post', 'rename_posts_labels');
	// function rename_posts_labels( $labels ){
	// 	$new = array(
	// 		'name'                  => 'News',
	// 		'singular_name'         => 'News',
	// 		'add_new'               => 'Add New',
	// 		'add_new_item'          => 'Add News Post',
	// 		'edit_item'             => 'Edit News Post',
	// 		'new_item'              => 'New News Post',
	// 		'view_item'             => 'View News Post',
	// 		'search_items'          => 'Search News',
	// 		'not_found'             => 'No news found.',
	// 		'not_found_in_trash'    => 'No News posts found in Trash.',
	// 		'parent_item_colon'     => '',
	// 		'all_items'             => 'All News',
	// 		'archives'              => 'News Archives',
	// 		'attributes'            => 'News Attributes',  
	// 		'insert_into_item'      => 'Insert into News post',
	// 		'uploaded_to_this_item' => 'Uploaded to this News post',
	// 		'featured_image'        => 'Featured Image',
	// 		'set_featured_image'    => 'Set featured image',
	// 		'remove_featured_image' => 'Remove featured image',
	// 		'use_featured_image'    => 'Use as featured image',
	// 		'filter_items_list'     => 'Filter News List',
	// 		'items_list_navigation' => 'News List Navigation',
	// 		'items_list'            => 'News List',
	// 		'menu_name'             => 'News',
	// 		'name_admin_bar'        => 'News', // пункте "добавить"
	// 	);

	// 	return (object) array_merge( (array) $labels, $new );
	// }
	function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
	}
	function revcon_change_post_object() {
	    global $wp_post_types;
	    $labels = &$wp_post_types['post']->labels;
	    $labels->name = 'News';
	    $labels->singular_name = 'News';
	    $labels->add_new = 'Add News';
	    $labels->add_new_item = 'Add News';
	    $labels->edit_item = 'Edit News';
	    $labels->new_item = 'News';
	    $labels->view_item = 'View News';
	    $labels->search_items = 'Search News';
	    $labels->not_found = 'No News found';
	    $labels->not_found_in_trash = 'No News found in Trash';
	    $labels->all_items = 'All News';
	    $labels->menu_name = 'News';
	    $labels->name_admin_bar = 'News';
	}
	 
	add_action( 'admin_menu', 'revcon_change_post_label' );
	add_action( 'init', 'revcon_change_post_object' );
    // rename post into news end

    // registering new post type Importaint News start
	function custom_post_type() {
	// Set UI labels for Custom Post Type
	    $labels = array(
	        'name'                => _x( 'Important News', 'Post Type General Name', 'company-th' ),
	        'singular_name'       => _x( 'Important News', 'Post Type Singular Name', 'company-th' ),
	        'menu_name'           => __( 'Important News', 'company-th' ),
	        'parent_item_colon'   => __( 'Parent Important News', 'company-th' ),
	        'all_items'           => __( 'All Important News', 'company-th' ),
	        'view_item'           => __( 'View Important News', 'company-th' ),
	        'add_new_item'        => __( 'Add New Important News', 'company-th' ),
	        'add_new'             => __( 'Add New', 'company-th' ),
	        'edit_item'           => __( 'Edit Important News', 'company-th' ),
	        'update_item'         => __( 'Update Important News', 'company-th' ),
	        'search_items'        => __( 'Search Important News', 'company-th' ),
	        'not_found'           => __( 'Not Found', 'company-th' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'company-th' ),
	    );
	     
	// Set other options for Custom Post Type
	     
	    $args = array(
	        'label'               => __( 'important_news', 'company-th' ),
	        'description'         => __( 'Important News', 'company-th' ),
	        'labels'              => $labels,
	        // Features this CPT supports in Post Editor
	        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
	        /* A hierarchical CPT is like Pages and can have
	        * Parent and child items. A non-hierarchical CPT
	        * is like Posts.
	        */ 
	        'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_nav_menus'   => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 3,
	        'can_export'          => true,
	        'has_archive'         => true,
	        'exclude_from_search' => false,
	        'publicly_queryable'  => true,
	        'capability_type'     => 'page',
	         // You can associate this CPT with a taxonomy or custom taxonomy. 
	        'taxonomies'          => array( 'category' ),
	    );
	     
	    // Registering your Custom Post Type
	    register_post_type( 'important_news', $args );
	 
	}
	 
	/* Hook into the 'init' action so that the function
	* Containing our post type registration is not 
	* unnecessarily executed. 
	*/
	add_action( 'init', 'custom_post_type', 0 );
	// Hooking up our function to theme 
	// registering new post type Importaint News end 
    // add post format to imp news
    function add_post_format_to_imp_news(){
		add_post_type_support('important_news', 'post-formats');
	}
    add_action( 'init', 'add_post_format_to_imp_news');

    //adding css styles
	function addStylesJs(){
		wp_enqueue_script('jquery');
		wp_enqueue_style('style', get_theme_file_uri( '/css/main.css'));
		wp_enqueue_style('style-med', get_theme_file_uri( '/css/medium.css'), array(), '1.0', 'screen and (max-width: 1325px)');
		wp_enqueue_style('style-phones', get_theme_file_uri( '/css/phones.css'), array(), '1.0', 'screen and (max-width: 1000px)');
		wp_enqueue_style('style-phones-large', get_theme_file_uri( '/css/phones-large.css'), array(), '1.0', 'screen and (max-width: 800px)');
		wp_enqueue_style('style-phones-medium', get_theme_file_uri( '/css/phones-medium.css'), array(), '1.0', 'screen and (max-width: 660px)');
		wp_enqueue_style('style-phones-small', get_theme_file_uri( '/css/phones-small.css'), array(), '1.0', 'screen and (max-width: 500px)');
		wp_enqueue_style('style-phones-thesmallest', get_theme_file_uri( '/css/phones-thesmallest.css'), array(), '1.0', 'screen and (max-width: 400px)');
		wp_enqueue_style('owl_style', get_theme_file_uri( '/css/owl-styles/owl.carousel.min.css'));
		wp_enqueue_script('font_js', get_template_directory_uri() . '/js/fontfaceobserver.standalone.js', array(), 1.0, false);
		wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.js', array(), 1.0, true);
	}
	add_action('wp_enqueue_scripts', 'addStylesJs');

	//Controll post length on the main page

	function set_excerpt_length(){
		if(is_front_page() && get_post_type() === 'important_news'){
			return has_post_thumbnail() ? 25 : 50;
		}
		else if(is_page('all-news')){
			return has_post_thumbnail() ? 65 : 80;
		}else{
			return has_post_thumbnail() ? 40 : 75;
		} 
	}
	add_filter('excerpt_length','set_excerpt_length');

	function custom_excerpt_read_more( $more ) {
		global $post;
	    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
	        get_permalink( get_the_ID() ),
	        __( '<span class="read-more__icon company comp-circle-right"></span>', 'company-th' )
	    );
	}
	add_filter( 'excerpt_more', 'custom_excerpt_read_more' );

	//widgets locations start
	function wpb_init_widgets($id){
		register_sidebar(array(
		    'name'=>'footerSearch',
		    'id'=> 'footer-search',
		    'before_widget'=> '<div class="footer-search">',
		    'after_widget'=> '</div>'
		));
	}
	add_action('widgets_init', 'wpb_init_widgets');
	//widgets locations end

	//search input start
    function wpdocs_after_setup_theme() {
	    add_theme_support( 'html5', array( 'search-form' ) );
	}
	add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
    //search input end

	// Add responsive container to embeds
 
	function alx_embed_html( $html ) {
	   return '<div class="video-container">' . $html . '</div>';
	}
	add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );

	// removing url field in comments section start
	function disable_url_in_comments_field($fields){
		if(isset($fields['url']))
		unset($fields['url']);
		return $fields;
	}
    add_filter('comment_form_default_fields', 'disable_url_in_comments_field');
    // removing url field in comments section end
    /**
	 * Hides the custom post template for pages on WordPress 4.6 and older
	 *
	 * @param array $post_templates Array of page templates. Keys are filenames, values are translated names.
	 * @return array Filtered array of page templates.
	 */
	function makewp_exclude_page_templates( $post_templates ) {
	    if ( version_compare( $GLOBALS['wp_version'], '4.7', '&lt;' ) ) {
	        unset( $post_templates['templates/my-full-width-post-template.php'] );
	    }
	  
	    return $post_templates;
	}
	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_template_directory_uri() . '/images/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" type="image/x-icon" sizes="32x32" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');